package models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anthony on 09/05/2016.
 */
public class Rain {

    @SerializedName("3h") private double precipitation;

    public double getPrecipitation() {
        return precipitation;
    }

    public void setPrecipitation(double precipitation) {
        this.precipitation = precipitation;
    }
}
