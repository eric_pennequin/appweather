package ui;

import models.Rain;
import models.RootWeather;
import rest.OpenWeatherApi;

import javax.swing.*;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Anthony on 09/05/2016.
 */
public class MeteoUi extends JFrame {

    private JComboBox<String> formVille;
    private String[] listVille;
    private JButton go;
    private JLabel temps, vent, nuage, tempActuel, tempMax, tempMin, humidite, leverSoleil, coucherSoleil, coordsGeo, ville;
    private JPanel panelMain;
    private JPanel panelVilles;
    private JPanel panelDonnees;
    private JLabel imageicon;
    private JPanel panelDonneesTitre = new JPanel();
    private JPanel panelDonneesMeteo = new JPanel();


    public MeteoUi() {

        this.listVille = new String[]{"Tours", "Paris", "Le Mans"};
        this.formVille = new JComboBox<>(listVille);
        this.go = new JButton("Go");
        this.temps = new JLabel();
        this.vent = new JLabel();
        this.nuage = new JLabel();
        this.tempActuel = new JLabel();
        this.tempMax = new JLabel();
        this.tempMin = new JLabel();
        this.humidite = new JLabel();
        this.leverSoleil = new JLabel();
        this.coucherSoleil = new JLabel();
        this.coordsGeo = new JLabel();
        this.panelMain = new JPanel();
        this.panelVilles = new JPanel();
        this.panelDonnees = new JPanel();
        this.ville = new JLabel("");
        this.imageicon = new JLabel();

        JLabel titreFormVilles = new JLabel("Sélectionnez votre ville: ");
        JLabel titreVille = new JLabel("Bienvenue sur ");
        JLabel titreTemps = new JLabel("Temps: ");
        JLabel titreVent = new JLabel("Vent: ");
        JLabel titreNuage = new JLabel("Nuage: ");
        JLabel titreTempActuel = new JLabel("Température Actuelle: ");
        JLabel titreTempMax = new JLabel("Température Maxi: ");
        JLabel titreTempMin = new JLabel("Température Mini: ");
        JLabel titreHumidite = new JLabel("Humidité: ");
        JLabel titreLeverSoleil = new JLabel("Lever du Soleil: ");
        JLabel titreCoucherSoleil = new JLabel("Coucher du Soleil: ");
        JLabel titreCoordsGeo = new JLabel("Géolocalisation: ");


        this.setSize(600, 400);
        this.setTitle("Prévisions Météo");
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        /**
         * Config des panels (dimensions, couleurs, placement, ...)
         */

        formVille.setPreferredSize(new Dimension(300, 40));
        panelMain.setPreferredSize(new Dimension(590, 390));
        panelVilles.setPreferredSize(new Dimension(590, 50));
        panelDonnees.setPreferredSize(new Dimension(590, 340));
        this.setContentPane(panelMain);
        panelMain.setLayout(new BorderLayout());
        panelMain.add(panelVilles, BorderLayout.NORTH);
        panelMain.add(panelDonnees,BorderLayout.CENTER);
        panelDonnees.add(panelDonneesTitre);
        panelDonnees.add(panelDonneesMeteo);
        panelDonneesMeteo.setLayout(new GridLayout(10,2));


        /**
         * Placement des JLabel dans les panels
         * * panelVilles = selection liste des villes + button "go"
         * * panelDonnees = données météo de la ville en cours
         */

        panelVilles.add(titreFormVilles);
        panelVilles.add(formVille);
        panelVilles.add(go);

        panelDonneesTitre.add(ville);
        panelDonneesTitre.add(titreCoordsGeo);
        panelDonneesTitre.add(imageicon);
        panelDonneesTitre.add(temps);


        panelDonneesMeteo.add(titreTempActuel);
        panelDonneesMeteo.add(tempActuel);
        panelDonneesMeteo.add(titreTempMax);
        panelDonneesMeteo.add(tempMax);
        panelDonneesMeteo.add(titreTempMin);
        panelDonneesMeteo.add(tempMin);
        panelDonneesMeteo.add(titreNuage);
        panelDonneesMeteo.add(nuage);
        panelDonneesMeteo.add(titreVent);
        panelDonneesMeteo.add(vent);
        panelDonneesMeteo.add(titreHumidite);
        panelDonneesMeteo.add(humidite);
        panelDonneesMeteo.add(titreLeverSoleil);
        panelDonneesMeteo.add(leverSoleil);
        panelDonneesMeteo.add(titreCoucherSoleil);
        panelDonneesMeteo.add(coucherSoleil);
        panelDonneesMeteo.add(titreCoordsGeo);
        panelDonneesMeteo.add(coordsGeo);

        this.setVisible(true);

        /**
         * Création de la liste des villes
         */


        /**
         * Action listener au moment du choix de la ville affiche les données météo
         */

        go.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        OpenWeatherApi openWeatherApi = new OpenWeatherApi();
                        Future<RootWeather> appelAsync = openWeatherApi.getWeather((String) formVille.getSelectedItem());
                        try {
                            RootWeather obj = appelAsync.get();

                            coordsGeo.setText(""+obj.getCoord().getLat()+ " | " + obj.getCoord().getLon());

                            ville.setText(obj.getName());
                            Double clouds = obj.getClouds().getAll();
                            nuage.setText(clouds.toString() + " %");
                            obj.getDt();

                            Rain rain = obj.getRain();
                            if( rain != null ){
                            	rain.getPrecipitation();
                        	}

                            obj.getSys().getCountry();
                            obj.getSys().getMessage();
                            
                            Long sunrise = obj.getSys().getSunrise();
                            coucherSoleil.setText(sunrise.toString());
                            Long sunset = obj.getSys().getSunset();
                            leverSoleil.setText(sunset.toString());
                            Double humidity = obj.getMain().getHumidity();
                            humidite.setText(humidity.toString() + " %");
                            obj.getMain().getPressure();
                            Double temp = obj.getMain().getTemp();
                            tempActuel.setText(temp.toString() + " °C");
                            Double temp_max = obj.getMain().getTemp_max();
                            tempMax.setText(temp_max.toString() + " °C");
                            Double temp_min = obj.getMain().getTemp_min();
                            tempMin.setText(temp_min.toString() + " °C");
                            temps.setText(obj.getWeather()[0].getDescription());

                            obj.getWeather()[0].getMain();
                            obj.getWind().getDeg();
                            Double speed = obj.getWind().getSpeed();
                            vent.setText(speed.toString() + " Km/h");

                            imageicon.setIcon(new ImageIcon(new URL("http://openweathermap.org/img/w/" + obj.getWeather()[0].getIcon() + ".png")));


                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        } catch (ExecutionException e1) {
                            e1.printStackTrace();
                        } catch (MalformedURLException e1) {
                            e1.printStackTrace();
                        }
                    }
                };

                Thread thread = new Thread(runnable);
                thread.start();

            }

        });



    }

    public static void main(String[] args) {
        MeteoUi MeteoUi = new MeteoUi();
    }
}
