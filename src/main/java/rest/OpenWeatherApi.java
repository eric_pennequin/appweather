package rest;

import com.google.gson.Gson;
import models.RootWeather;
import org.asynchttpclient.*;

import java.util.concurrent.Future;

/**
 * Created by Anthony on 09/05/2016.
 */
public class OpenWeatherApi {

    public Future<RootWeather> getWeather(String street) {
        AsyncHttpClient asyncHttpClient = new DefaultAsyncHttpClient();
        return asyncHttpClient.prepareGet("http://api.openweathermap.org/data/2.5/weather?q="+street+"&APPID=014338d0063abb9e17042b84f7a50b6a&lang=fr&units=metric").execute(
                new AsyncCompletionHandler<RootWeather>() {

            @Override
            public RootWeather onCompleted(Response response) throws Exception {
                // Do something with the Response
                // ...
                String data = response.getResponseBody();
                Gson gson = new Gson();
                RootWeather obj = gson.fromJson(data, RootWeather.class);

                return obj;
            }

            @Override
            public void onThrowable(Throwable t) {
                // Something wrong happened.
            }
        });
    }

}
